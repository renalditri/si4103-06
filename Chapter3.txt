3.1 Perancangan Fitur 
	Berikut adalah fitur-fitur yang disediakan Aplikasi Hallopakar:
	1. Login
	2. Search Pakar
	3. Menampilkan list-list pakar
	4. Chatting 
	5. Logout

3.2 Perancangan Basis Data
	Database akan dirancang agar dapat menampung data dari user baik itu pakar atau pelajar itu sendiri. Secara garis besar, data yang akan disimpan di dalam database ialah data transaksi, data statistik, dan data lainnya. Database yang akan digunakan di aplikasi ini adalah Oracle.


3.3 Perancangan Antar Muka
	Perancangan antar muka dari aplikasi HaloPakar ini akan dibuat mudah dimengerti 
dan simple. Dengan begitu memberikan sebuah kemudahan bagi user untuk dapat berinterkasi dengan program.


3.4 Perancangan Sistem Operasi
	Sistem operasi dari aplikasi halopakar hanya bisa untuk android dengan versi KitKat keatas saja