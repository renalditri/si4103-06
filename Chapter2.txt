2.1 Language Program 
	Dalam membangun aplikasi ini, developer menggunakan bahasa pemrograman java dengan output berupa 
aplikasi berbasis android. Tools yang digunakan untuk membangun dan mewujudkan aplikasi ini adalah Android Studio.
	Sistem pengerjaan kelompok menggunakan git sebagai sarana untuk pengendali revisi dan sarana untuk mengerjakan 
proyek aplikasi Hallopakar ini.

2.2 Versioning Control
	Version OS yang diperlukan agar HaloPakar berjalan dengan baik ialah Minimal Android KitKat+ dan tidak mensupport iOS. Selanjutnya akan dilakukan update pada aplikasi agar dapat mengikuti perkembangan version OS yang akan ada pada masa mendatang secara perlahan.

2.3 Database Dan Koneksi Data
 HaloPakar ini menggunakan database MySql hingga lebih portable dan mudah diterapkan oleh pengguna aplikasi yang awam sekalipun.
Secara teknis transaksi dengan pakar termasuk dalam kelompok skala kecil,sehingga lebih tepat ditangani secara mudah dengan database ini.

2.4 User requirement
	Dalam aplikasi ini requirement user yaitu memiliki akun agar bisa login yang akunnya berfungsi untuk menyimpan data dari
sang pakar


