1. Fase Analisis	: Dilaksanakan selama 2 minggu bagi tim system analyst untuk melaksanakan tugas.
2. Fase Desain		: Diberikan waktu untuk menyelesaikan tiap desain selama 2-3 minggu tergantung kompleksitas dan kebutuhan pengguna.
3. Fase Development	: Pelaksanaan pengembangan aplikasi akan diberikan jangka waktu selama 3 bulan berdampingan dengan fase desain agar desain bisa langsung dikembangkan dan juga untuk menggabungkan semua fitur.
4. Fase Testing		: Testing dilakukan secara ideal selama 1-2 minggu tanpa kendala yang fatal. Bisa diperpanjang menjadi 1 bulan bila ditemukan banyak masalah.
5. Fase Maintenance	: Fase ini akan terus berjalan selama aplikasi masih berfungsi dan digunakan atau hingga client ingin memberhentikan jalannya aplikasi.

Jadwal bersifat tentatif dan akan disesuaikan lagi dengan jalannya pengembangan. Runtutan jadwal tidak selalu berurutan karena desain dan pengembangan bisa dilakukan secara bersamaan bila dibutuhkan.